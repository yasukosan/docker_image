FROM    node:12.13 as node
FROM    php:7.3-fpm

ARG USER="www-data"
ARG UID="1000"
ARG GROUP="www-data"
ARG GID="1000"

# timezone (Asia/Tokyo)
ENV TZ JST-9
ENV TERM xterm

RUN     usermod -u ${UID} ${USER}        \
    &&  groupmod -g ${GID} ${GROUP}       \
    &&  pecl install xdebug-2.7.2       \
    &&  apt-get update -y               \
    &&  apt-get -y install git iproute2 procps iproute2 lsb-release \
    &&  apt-get install -y libfreetype6-dev libjpeg62-turbo-dev libpng-dev libzip-dev   \
    &&  docker-php-ext-enable xdebug    \
    &&  docker-php-ext-install pdo_mysql mysqli mbstring zip gd \
    &&  apt-get clean
RUN cd ~
RUN curl -sS https://getcomposer.org/installer -o composer-setup.php
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer
COPY    docker-php-ext-xdebug.ini /usr/local/etc/php/conf.d/

COPY ./php.ini /etc/php/7.3/fpm/php.ini

# Copy NodeJS binaries
COPY --from=node /usr/local/bin/node /usr/local/bin/
COPY --from=node /usr/local/lib/node_modules/ /usr/local/lib/node_modules/
RUN ln -s /usr/local/bin/node /usr/local/bin/nodejs \
    && ln -s /usr/local/lib/node_modules/npm/bin/npm-cli.js /usr/local/bin/npm \
    && ln -s /usr/local/lib/node_modules/npm/bin/npm-cli.js /usr/local/bin/npx

# Copy php composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

COPY ./setup.sh /usr/local/bin/setup.sh
RUN chmod +x /usr/local/bin/setup.sh
# RUN sudo -u user sh /usr/local/bin/setup.sh


