#!/bin/bash

# Permission Change
chmod -R 777 /var/www/html/storage/logs

# env file copy
ENV="/var/www/html/.env"
if [ ! -e $ENV ];then
    cp /var/www/html/.env.example /var/www/html/.env
fi

# check sql dump
RES=`find /tmp/db-entrypoint -maxdepth 1 -name *.sql 2>/dev/null`
while :
do
    P0=`telnet db 3306 &`
    sleep 1
    P1=`pgrep -u root telnet`
    P2=`ps aux | grep telnet | grep -v grep | wc -l`
    echo $P0
    if [[ "$P0" =~ "Connected" ]]; then
        kill -9 $P1
        break
    fi
done

if [ $? -ne 0]; then
    echo "Another Trable"
elif [ -z "$RES" ]; then
    cd /var/www/html
    composer install
    composer dump-autoload
    /usr/local/bin/php ./artisan migrate
    /usr/local/bin/php ./artisan db:seed
    echo "Do laravel migration"
else
    echo "Restore dump data"
fi